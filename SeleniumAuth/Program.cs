﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace SeleniumAuth
{
    class Program
    {
        static string cookies;        

        static Dictionary<string, Tuple<string, string>> credentials = new Dictionary<string, Tuple<string, string>>
        {
            {"github", new Tuple<string, string>("githubfaketesting", "123GitHubFake")},
            {"bitbucket", new Tuple<string, string>("no2fatestuser@gmail.com", "123456test")}
        };

        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Please pass 2 keys as command line arguments");                
                return;
            }

            var gitHubCreds = GetCredentials(args[0]);
            var bitBucketCreds = GetCredentials(args[1]);

            if (gitHubCreds == null || bitBucketCreds == null)
            {
                Console.WriteLine("Please pass correct keys");                
                return;
            }

            Console.WriteLine("*************");
            Console.WriteLine("Keys accepted");
            Console.WriteLine("*************");

            //to run on local machine path to chromedriver should be passed
            //var pathToChomedirver = Environment.ExpandEnvironmentVariables(@"%userprofile%\desktop\chromedriver");
            //IWebDriver driver = new ChromeDriver(pathToChomedirver);

            IWebDriver driver = new ChromeDriver();

            driver.Url = "https://github.com/login";
            IWebElement logingh = driver.FindElement(By.Id("login_field"));
            logingh.SendKeys(gitHubCreds.Item1);
            IWebElement passwordgh = driver.FindElement(By.Id("password"));
            passwordgh.SendKeys(gitHubCreds.Item2);
            IWebElement signInButtongh = driver.FindElement(By.Name("commit"));
            signInButtongh.Click();

            driver.Url = "https://bitbucket.org/account/signin/";
            IWebElement loginbb = driver.FindElement(By.Name("username"));
            loginbb.SendKeys(bitBucketCreds.Item1);
            IWebElement passwordbb = driver.FindElement(By.Name("password"));
            passwordbb.SendKeys(bitBucketCreds.Item2);
            IWebElement signInButtonbb = driver.FindElement(By.XPath(".//*[@id='aid-login-form']/div[2]/input"));
            signInButtonbb.Click();

            driver.Url = "chrome://version";
            IWebElement path = driver.FindElement(By.Id("profile_path"));
            var settingsPath = path.Text;
            cookies = Path.Combine(settingsPath, "Cookies");

            var destinationPath = Environment.ExpandEnvironmentVariables(@"%localappdata%\Google\Chrome\User Data\Default\Cookies");

            waitCookies();

            File.Copy(cookies, destinationPath, true);

            driver.Quit();
        }

        static Tuple<string, string> GetCredentials(string key)
        {
            Tuple<string, string> creds;
            credentials.TryGetValue(key, out creds);

            return creds;
        }

        static bool CookieNotYetUpdated()
        {
            double cookiesSize = (new FileInfo(cookies).Length) / 1024;
            if (cookiesSize < 35)
            {
                return true;
            }

            return false;
        }

        static void waitCookies()
        {
            Console.WriteLine("*******************************");
            Console.WriteLine("Generating Cookies, please wait");
            Console.WriteLine("*******************************");

            var secondsToWait = 60;
            Stopwatch s = new Stopwatch();
            s.Start();

            do
            {
                Thread.Sleep(1000);
            } while (CookieNotYetUpdated() && s.Elapsed < TimeSpan.FromSeconds(secondsToWait));

            s.Stop();
        }
    }
}
